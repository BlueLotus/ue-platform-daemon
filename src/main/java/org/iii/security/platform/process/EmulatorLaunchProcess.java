package org.iii.security.platform.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class EmulatorLaunchProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(EmulatorLaunchProcess.class); 
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	public EmulatorLaunchProcess(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
	}
	public int launchEmulator(long emulatorEnvId, EmulatorEnvRepository emulatorEnvRepository) {
		logger.debug("Emulator launching...");
		try {
			EmulationEnv env = emulatorEnvRepository.findOne(emulatorEnvId);
			processBuilderGenerator.setEmulatorName(env.getEmulatorName());
			ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_EMULATOR_LAUNCH_PROCESS);
			Process process = processBuilder.start();
			env.setEmulatorLaunched(true);
			emulatorEnvRepository.save(env);
			
			logger.debug("Emulator launched...");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            
            String processContent;
            logger.debug("Process content: ");
            while ((processContent = stdInput.readLine()) != null) {
                System.out.println(processContent);
            }
            logger.debug("Process error content: ");
            while ((processContent = stdError.readLine()) != null) {
                System.out.println(processContent);
            }
            logger.debug("Emulator shutdown...");
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		return 1;
	}
	
}
