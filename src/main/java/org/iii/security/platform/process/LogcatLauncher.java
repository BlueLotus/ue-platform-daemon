package org.iii.security.platform.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class LogcatLauncher {
	
	private static final Logger logger = LoggerFactory.getLogger(LogcatLauncher.class);
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	public LogcatLauncher(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
	}
	
	public boolean launchLogcat(long id, EmulatorEnvRepository emulatorEnvRepository) {
		boolean launchResult = false;
		if(emulatorIsLaunched()) {
			LogcatLaunchProcess logcatLaunchProcess = new LogcatLaunchProcess(processBuilderGenerator);
			launchResult = logcatLaunchProcess.launch(id, emulatorEnvRepository);
		}
		return launchResult;
	}
	
	private boolean emulatorIsLaunched() {
		boolean launched = false;
		logger.debug("	Detecting the emulator is launched or not...");
		try {
			ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_LOGCAT_LAUNCHER);
			Process process = processBuilder.start();
			
			logger.debug("	Checking emulator list start...");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String emulatorDetectionProcessContent;
            logger.debug("		Process content: ");
            while ((emulatorDetectionProcessContent = stdInput.readLine()) != null) {
                logger.debug("			{}", emulatorDetectionProcessContent);
                if(emulatorDetectionProcessContent.contains("emulator") && emulatorDetectionProcessContent.contains("device")) {
                	logger.debug("			-> Emulator has launched.");
                	launched = true;
                } else {
                	logger.debug("			-> Emulator hasn't launched.");
                }
            }
            logger.debug("		Process error content: ");
        	while ((emulatorDetectionProcessContent = stdError.readLine()) != null) {
        		logger.debug("{}", emulatorDetectionProcessContent);
        	}
            logger.debug("	Checking emulator list end...");
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		return launched;
	}

}
