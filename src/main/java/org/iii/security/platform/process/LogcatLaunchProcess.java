package org.iii.security.platform.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.iii.security.platform.util.UEPlatformDaemonTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class LogcatLaunchProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(LogcatLaunchProcess.class);

	private static final String BOOT_COMPLETED_MESSAGE = "Action = android.intent.action.BOOT_COMPLETED";
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	public LogcatLaunchProcess(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
	}
	
	public boolean launch(long id, EmulatorEnvRepository emulatorEnvRepository) {
		boolean launchedAndTerminatedNormally = false;
		logger.debug("	Starting logcat...");
		try {
			ProcessBuilder launchLogcatProcessBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_LOGCAT_LAUNCH_PROCESS);
			Process process = launchLogcatProcessBuilder.start();
			EmulationEnv env = emulatorEnvRepository.findOne(id);
			env.setLogcatLaunched(true);
			emulatorEnvRepository.save(env);
			
			logger.debug("	Logcat launched...");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            
            String launchLogcatProcessContent;
            logger.debug("		Process content: ");
            FileOutputStream file = null;
            file =  new FileOutputStream(new File(UEPlatformDaemonTool.generateLogNameForEmulator(env)), true);
            while ((launchLogcatProcessContent = stdInput.readLine()) != null) {
               if(launchLogcatProcessContent.contains(BOOT_COMPLETED_MESSAGE)) {
            	   env.setCanInstallApk(true);
            	   emulatorEnvRepository.save(env);
            	   logger.debug("Emulator has boot completed!");
               }
               System.out.println("			" + launchLogcatProcessContent);
               file.write(launchLogcatProcessContent.concat("\n").getBytes(Charset.forName("UTF-8")));
            }
            logger.debug("	Logcat has terminated...");
            file.close();
            launchedAndTerminatedNormally = true;
            logger.debug("		Process error content: ");
            while ((launchLogcatProcessContent = stdError.readLine()) != null) {
                logger.debug("			{}", launchLogcatProcessContent);
            }
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return launchedAndTerminatedNormally;
	}
	
}
