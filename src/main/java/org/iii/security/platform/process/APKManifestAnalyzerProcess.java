package org.iii.security.platform.process;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.constant.UEPlatformConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.iii.security.platform.core.util.xml.AndroidManifestAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class APKManifestAnalyzerProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(APKManifestAnalyzerProcess.class);
	
	private AndroidManifestAnalyzer androidManifestAnalyzer;
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	private String packageName;
	
	private String usesPermission;
	
	public APKManifestAnalyzerProcess(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
	}
	
	public void decompileAPK(String apkName) {
		try {
			processBuilderGenerator.setApkName(apkName);
			ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_APK_MANIFEST_ANALYZER_PROCESS);
			Process process = processBuilder.start();
			logger.debug("Start to decompile APK: {}", apkName);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			while (reader.readLine() != null) {
			}
			logger.debug("APK: {} has decompiled successfully.", apkName);
		} catch (Exception e) {
			logger.debug("Something wrong when decompile APK: {}", apkName);
		}
	}
	
	public void extractAndroidManifest(String apkName) {
		androidManifestAnalyzer = new AndroidManifestAnalyzer();
		androidManifestAnalyzer.loadManifest(UEPlatformConstant.DECOMPILED_APK_FILE_LOCATION + apkName + "\\AndroidManifest.xml");
		usesPermission = androidManifestAnalyzer.transformToUsesPermissionString(androidManifestAnalyzer.extractAllPermissions());
		packageName = androidManifestAnalyzer.getPackageName();
	}

	public String getPackageName() {
		return packageName;
	}

	public String getUsesPermission() {
		return usesPermission;
	}

}
