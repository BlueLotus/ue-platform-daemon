package org.iii.security.platform.process;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class APKFileInstallerProcess {

	private static final Logger logger = LoggerFactory.getLogger(APKFileInstallerProcess.class);
	
	private static final int MAX_ATTEMPT = 2;
	private static final String PACKAGE_MANAGER_NOT_RUNNING = "Error: Could not access the Package Manager.  Is the system running?";
	private static final String APK_ALREADY_INSTALLED = "Failure [INSTALL_FAILED_ALREADY_EXISTS]";
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	private String packageName;
	
	private int attempt; 
	
	private int installedResult;
	
	public APKFileInstallerProcess(ProcessBuilderGenerator processBuilderGenerator, String packageName) {
		this.processBuilderGenerator = processBuilderGenerator;
		this.packageName = packageName;
		this.attempt = 0;
		this.installedResult = EmulatorJobConstant.INSTALL_FAILED;
	}
	
	public int install(String apkName) {
		logger.debug("	Installing apk: {}...", apkName);
		do {
			try {
				processBuilderGenerator.setApkName(apkName);
				ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_APK_FILE_INSTALLER_PROCESS);
				Process process = processBuilder.start();
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
	            String installApkProcessContent;
	            logger.debug("		Process content: ");
	            while ((installApkProcessContent = stdInput.readLine()) != null) {
	            	if(installApkProcessContent.contains(PACKAGE_MANAGER_NOT_RUNNING)) {
	            		installedResult = EmulatorJobConstant.INSTALL_FAILED_BECAUSE_NO_AVAILABLE_PACKAGE_MANAGER;
	            	} else if(installApkProcessContent.contains(APK_ALREADY_INSTALLED)) {
	            		installedResult = EmulatorJobConstant.INSTALL_FAILED_BECAUSE_APK_ALREADY_INSTALLED;
	            	} else if(installApkProcessContent.contentEquals("Success") ) {
	            		installedResult = EmulatorJobConstant.INSTALL_SUCCESSFULLY;
	            	}
	                logger.debug("			{}", installApkProcessContent);
	            }
	            if(installedResult == EmulatorJobConstant.INSTALL_FAILED)
	            	logger.debug("		Installation failed...");
	            else if(installedResult == EmulatorJobConstant.INSTALL_FAILED_BECAUSE_NO_AVAILABLE_PACKAGE_MANAGER)
	            	logger.debug("		The package manager hasn't ready yet...");
	            else if(installedResult == EmulatorJobConstant.INSTALL_FAILED_BECAUSE_APK_ALREADY_INSTALLED) {
	            	if(rescuable()) {
	            		logger.debug("		The apk has already installed, reinstall now...");
	            		processBuilderGenerator.setPackageName(packageName);
	            		ProcessBuilder deleteProcessBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_APK_FILE_UNINSTALLER_PROCESS);
	            		Process delete = deleteProcessBuilder.start();
	            		BufferedReader deleteProcessReader = new BufferedReader(new InputStreamReader(delete.getInputStream()));
	            		String deleteMsg;
	            		logger.debug("		Deleting existed APK file: {} ...", apkName);
	            		while((deleteMsg = deleteProcessReader.readLine()) != null) {
	            			logger.debug("			{}", deleteMsg);
	            		}
	            	} else
	            		logger.debug("		Already reinstalled for {} times but still failed, please contact the system admin.", MAX_ATTEMPT);
	            } else if(installedResult == EmulatorJobConstant.INSTALL_SUCCESSFULLY)
	            	logger.debug("		The apk {} has installed successfully...", apkName);
			} catch (Exception e) {
				logger.debug(e.getMessage());
			}
		} while (isRecurrent());
		return installedResult;
	}
	
	private boolean rescuable() {
		return attempt < MAX_ATTEMPT;
	}
	
	private boolean isRecurrent() {
		return installedResult == EmulatorJobConstant.INSTALL_FAILED_BECAUSE_APK_ALREADY_INSTALLED && rescuable();
	}
	
}
