package org.iii.security.platform.util;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.util.constant.UEPlatformConstant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformDaemonTool {

	public static String generateLogNameForEmulator(EmulationEnv env) {
		return UEPlatformConstant.LOGCAT_LOG_LOCATION + String.valueOf(env.getAnalyticsProjectId()).concat("_").concat(env.getApkForAnalytics()).concat("_analysis_log.txt");
	}
	
	public static String generateLogNameForProject(Project project) {
		return UEPlatformConstant.LOGCAT_LOG_LOCATION + String.valueOf(project.getId()).concat("_").concat(project.getApkName()).concat("_analysis_log.txt");
	}
	
}
