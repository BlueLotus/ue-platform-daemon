package org.iii.security.platform.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.LogEntry;
import org.iii.security.platform.core.domain.Project;
import org.json.JSONArray;
import org.springframework.stereotype.Component;

/**
 * 
 * @author KaKa, Carl Adler(C.A.)
 *
 */
@Component
public class TaintdroidLogParser {

	private static final String GENERAL_COLOR = "#00b8d4";
	private static final String DANGER_COLOR = "#e00032";
	
	private String projectId;
	private String logName;
	
	/*public static void main(String[] args) {
		try {
		TaintdroidLogParser logParser = new TaintdroidLogParser();
		logParser.projectId = "100";
		logParser.logName = "C:\\UE_analysis\\analysis_log\\logcat.txt";
			System.out.println(logParser.parseLog());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public void initForEmulator(EmulationEnv env) {
		this.projectId = String.valueOf(env.getAnalyticsProjectId());
		this.logName = UEPlatformDaemonTool.generateLogNameForEmulator(env);
	}
	
	public void initForProject(Project project) {
		this.projectId = String.valueOf(project.getId());
		this.logName = UEPlatformDaemonTool.generateLogNameForProject(project);
	}

	public String parseLog() throws Exception {
		FileReader fr = new FileReader(logName);
		BufferedReader br = new BufferedReader(fr);
		String logContent = null;
		HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>> taintHashmap = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>>();
		String tempPackagename = null;
		String packagename = null;
		String activity = null;

		while (br.ready())
			logContent += br.readLine() + "\n";
		fr.close();
		String[] splitLogContentByActivity = logContent
				.split("\\d{2}-\\d{2} \\d{2}\\:\\d{2}\\:\\d{2}\\.\\d{3} I\\/ActivityManager\\(  \\d{1,}\\)\\: START u0");
		for (String activityContent : splitLogContentByActivity) {
			String[] spiltContentBySpace = activityContent.split("\n");

			for (String spaceContent : spiltContentBySpace) {
				if (spaceContent.contains("cmp")) {
					String[] temp = spaceContent.split("/");
					if (temp[0].length() > 0) {
						String[] apptemp = temp[0].split("cmp=");
						if (apptemp.length > 1) {
							tempPackagename = apptemp[1];
						}
					}
					if (temp[1].indexOf("}") > 0) {
						activity = temp[1].substring(0, temp[1].indexOf("}")).split(" ")[0];
					}
				}
				// 看Http Log
				if (spaceContent.contains("TaintLog") && spaceContent.contains("libcore.os.send")) {
					packagename = tempPackagename;
					String stratStr = "libcore.os.send(";
					String endStr = ")";
					int spos = spaceContent.indexOf(stratStr);
					int epos = spaceContent.indexOf(endStr, spos);
					String ip = spaceContent.substring(spos + stratStr.length(), epos);

					stratStr = "data=[";
					endStr = " ";
					int methodspos = spaceContent.indexOf(stratStr);
					int methodepos = spaceContent.indexOf(endStr, methodspos);
					String tempmethod = spaceContent.substring(methodspos + stratStr.length(), methodepos);
					String method = "Internet(" + getHttpMethod("http", tempmethod) + ")";

					stratStr = "tag ";
					endStr = "data=";
					int tagspos = spaceContent.indexOf(stratStr);
					int tagepos = spaceContent.indexOf(endStr);
					String tag = spaceContent.substring(tagspos + stratStr.length(), tagepos);

					String[] timetmep = spaceContent.split(" ");

					String detailString = spaceContent.substring(tagepos + 5);
					String[] spiltTag = getMultipleTag(tag.trim()).split(",");
					for (String tagtemp : spiltTag) {
						LogEntry le = new LogEntry(packagename, activity, tagtemp, method, ip, timetmep[0], timetmep[1], detailString);
						putTree(taintHashmap, le);
					}
				}
				// 看Https Log
				if (spaceContent.contains("TaintLog") && spaceContent.contains("SSLOutputStream.write")) {
					packagename = tempPackagename;
					String stratStr = "SSLOutputStream.write(";
					String endStr = ")";
					int spos = spaceContent.indexOf(stratStr);
					int epos = spaceContent.indexOf(endStr, spos);
					String ip = spaceContent.substring(spos + stratStr.length(), epos);

					stratStr = "data=[";
					endStr = " ";
					int methodspos = spaceContent.indexOf(stratStr);
					int methodepos = spaceContent.indexOf(endStr, methodspos);
					if (methodepos < 0) {
						methodepos = spaceContent.length() - 1;
					}
					String tempmethod = spaceContent.substring(methodspos + stratStr.length(), methodepos);
					String method = "Internet(" + getHttpMethod("https", tempmethod) + ")";

					stratStr = "tag ";
					endStr = "data=";
					int tagspos = spaceContent.indexOf(stratStr);
					int tagepos = spaceContent.indexOf(endStr);
					String tag = spaceContent.substring(tagspos + stratStr.length(), tagepos);

					String[] timetmep = spaceContent.split(" ");
					String detailString = spaceContent.substring(tagepos + endStr.length());
					String[] spiltTag = getMultipleTag(tag.trim()).split(",");
					for (String tagtemp : spiltTag) {
						LogEntry le = new LogEntry(packagename, activity, tagtemp, method, ip, timetmep[0], timetmep[1], detailString);
						putTree(taintHashmap, le);
					}
				}
			}
		}
		return getTreeJson(taintHashmap);
	}

	public String getHttpMethod(String httporhttps, String httpMethod) {
		if (httporhttps.equals("http")) {
			switch (httpMethod) {
			case "POST":
				return "HTTP/POST";
			case "GET":
				return "HTTP/GET";
			default:
				return "SOCKET";
			}
		} else {
			switch (httpMethod) {
			case "POST":
				return "HTTPS/POST";
			case "GET":
				return "HTTPS/GET";
			default:
				return "OTHER";
			}
		}
	}

	public String getMultipleTag(String multipletag) {
		int taint;
		// 0x400 -> 400
		String match = multipletag.substring(2);
		try {
			taint = Integer.parseInt(match, 16);
		} catch (NumberFormatException e) {
			return "Unknown Taint: " + match;
		}

		if (taint == 0x0) {
			return "No taint";
		}

		// for each taint
		ArrayList<String> list = new ArrayList<String>();
		int t;
		String tag;
		Hashtable<Integer, String> ttable = initializeTagTable();
		// check each bit
		for (int i = 0; i < 32; i++) {
			t = (taint >> i) & 0x1;
			tag = ttable.get(new Integer(t << i));
			if (tag != null) {
				list.add(tag);
			}
		}

		// build output
		StringBuilder sb = new StringBuilder("");
		if (list.size() > 1) {
			for (int i = 0; i < list.size() - 1; i++) {
				sb.append(list.get(i) + ",");
			}
			sb.append(list.get(list.size() - 1));
		} else {
			if (!list.isEmpty()) {
				sb.append(list.get(0));
			}
		}

		return sb.toString();
	}

	public Hashtable<Integer, String> initializeTagTable() {
		Hashtable<Integer, String> ttable = new Hashtable<Integer, String>();
		// ttable.put(new Integer(0x00000000), "No taint");
		ttable.put(new Integer(0x00000001), "Location");
		ttable.put(new Integer(0x00000002), "Address Book (ContactsProvider)");
		ttable.put(new Integer(0x00000004), "Microphone Input");
		ttable.put(new Integer(0x00000008), "Phone Number");
		ttable.put(new Integer(0x00000010), "GPS Location");
		ttable.put(new Integer(0x00000020), "NET-based Location");
		ttable.put(new Integer(0x00000040), "Last known Location");
		ttable.put(new Integer(0x00000080), "camera");
		ttable.put(new Integer(0x00000100), "accelerometer");
		ttable.put(new Integer(0x00000200), "SMS");
		ttable.put(new Integer(0x00000400), "IMEI");
		ttable.put(new Integer(0x00000800), "IMSI");
		ttable.put(new Integer(0x00001000), "ICCID (SIM card identifier)");
		ttable.put(new Integer(0x00002000), "Device serial number");
		ttable.put(new Integer(0x00004000), "User account information");
		ttable.put(new Integer(0x00008000), "browser history");
		return ttable;
	}

	public void putTree(HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>> taintHashmap, LogEntry logEntry) {
		String packageName = logEntry.getAppName();
		String activity = logEntry.getActivity();
		String tag = logEntry.getTag();
		String method = logEntry.getMethod();
		String tableheader = "<th>日期</th><th>時間</th><th colspan=\"4\">資料</th>";
		String tablerow = "<tr><td>" + logEntry.getDate() + "</td><td>" + logEntry.getTime() + "</td><td align=\"center\" colspan=\"6\">" + logEntry.getDetail() + "</td></tr>";
		String endNode = logEntry.getIp() + "\t" + tableheader + tablerow;
		// 丟值
		if (taintHashmap.containsKey(projectId)) {
			HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>> hashmap3 = taintHashmap.get(projectId);
			if (hashmap3.containsKey(packageName)) {
				HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> hashmap2 = hashmap3.get(packageName);
				if (hashmap2.containsKey(activity)) {
					HashMap<String, HashMap<String, ArrayList<String>>> hashmap1 = hashmap2.get(activity);
					if (hashmap1.containsKey(tag)) {
						HashMap<String, ArrayList<String>> hashmap = hashmap1.get(tag);
						if (hashmap.containsKey(method)) {
							ArrayList<String> list = hashmap.get(method);
							for (String endNodeList : list) {
								if (endNodeList.split("\t")[0].equals(logEntry.getIp())) {
									list.remove(endNodeList);
									tablerow = "<tr><td>"
											+ logEntry.getDate()
											+ "</td><td>"
											+ logEntry.getTime()
											+ "</td><td align=\"center\" colspan=\"6\">"
											+ logEntry.getDetail()
											+ "</td></tr>";
									list.add(endNodeList + tablerow);
								}
							}
							hashmap.put(method, list);
						} else {
							ArrayList<String> newList = new ArrayList<String>();
							newList.add(endNode);
							hashmap.put(method, newList);
						}
					} else {
						HashMap<String, ArrayList<String>> newHashmap = new HashMap<String, ArrayList<String>>();
						ArrayList<String> newList = new ArrayList<String>();
						newList.add(endNode);
						newHashmap.put(method, newList);
						hashmap1.put(tag, newHashmap);
					}
				} else {
					HashMap<String, HashMap<String, ArrayList<String>>> newHashmap1 = new HashMap<String, HashMap<String, ArrayList<String>>>();
					HashMap<String, ArrayList<String>> newHashmap = new HashMap<String, ArrayList<String>>();
					ArrayList<String> newList = new ArrayList<String>();
					newList.add(endNode);
					newHashmap.put(method, newList);
					newHashmap1.put(tag, newHashmap);
					hashmap2.put(activity, newHashmap1);
				}
			} else {
				HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> newHashmap2 = new HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>();
				HashMap<String, HashMap<String, ArrayList<String>>> newHashmap1 = new HashMap<String, HashMap<String, ArrayList<String>>>();
				HashMap<String, ArrayList<String>> newHashmap = new HashMap<String, ArrayList<String>>();
				ArrayList<String> newList = new ArrayList<String>();
				newList.add(endNode);
				newHashmap.put(method, newList);
				newHashmap1.put(tag, newHashmap);
				newHashmap2.put(activity, newHashmap1);
				hashmap3.put(packageName, newHashmap2);
			}
		} else {
			HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>> newHashmap3 = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>();
			HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> newHashmap2 = new HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>();
			HashMap<String, HashMap<String, ArrayList<String>>> newHashmap1 = new HashMap<String, HashMap<String, ArrayList<String>>>();
			HashMap<String, ArrayList<String>> newHashmap = new HashMap<String, ArrayList<String>>();
			ArrayList<String> newList = new ArrayList<String>();
			newList.add(endNode);
			newHashmap.put(method, newList);
			newHashmap1.put(tag, newHashmap);
			newHashmap2.put(activity, newHashmap1);
			newHashmap3.put(packageName, newHashmap2);
			taintHashmap.put(projectId, newHashmap3);
		}
	}

	public String getTreeJson(HashMap<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>> taintPackageList) {
		String picpath = "http://localhost:8080/ue-platform/analysis/activity/snapshot/projectId/" + projectId + "/activityName/";
		JSONArray jsonArray = new JSONArray();
		for (Map.Entry<String, HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>>> taintpackage : taintPackageList.entrySet()) {
			String idcolor = GENERAL_COLOR;
			JSONArray packageArray = new JSONArray();
			for (Map.Entry<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>> entry : taintpackage.getValue().entrySet()) {
				String packagecolor = GENERAL_COLOR;
				JSONArray appArray = new JSONArray();
				for (Map.Entry<String, HashMap<String, HashMap<String, ArrayList<String>>>> entry0 : entry.getValue().entrySet()) {
					String activitycolor = GENERAL_COLOR;
					String pic = picpath + entry0.getKey() + "/";
					JSONArray activityArray = new JSONArray();
					for (Map.Entry<String, HashMap<String, ArrayList<String>>> entry1 : entry0.getValue().entrySet()) {
						String datacolor = GENERAL_COLOR;
						JSONArray dataArray = new JSONArray();
						for (Map.Entry<String, ArrayList<String>> entry2 : entry1.getValue().entrySet()) {
							String methodcolor = GENERAL_COLOR;
							JSONArray methodArray = new JSONArray();
							for (String list : entry2.getValue()) {
								String endcolor = GENERAL_COLOR;
								String[] temp = list.split("\t");
								String parent = entry.getKey() + "/"
										+ entry0.getKey() + "/"
										+ entry1.getKey() + "/"
										+ entry2.getKey() + "/" + temp[0];
								if (temp.length > 1) {// 判斷是否有Detail內容
									if (temp[0].contains("140")) {// 判斷黑名單
										endcolor = DANGER_COLOR;
										methodcolor = DANGER_COLOR;
										datacolor = DANGER_COLOR;
										activitycolor = DANGER_COLOR;
										packagecolor = DANGER_COLOR;
										methodArray.put(getTreeEndNode(temp[0], null, 10, "white", endcolor, parent, temp[1], pic));
									} else {
										endcolor = GENERAL_COLOR;
										methodArray.put(getTreeEndNode(temp[0], null, 10, "white", endcolor, parent, temp[1], pic));
									}
								} else
									methodArray.put(getTreeNode(temp[0], null, 10, "white", endcolor, null));
							}
							dataArray.put(getTreeNode(entry2.getKey(), methodArray, 10, "white", methodcolor, null));
						}
						activityArray.put(getTreeNode(entry1.getKey(), dataArray, 10, "white", datacolor, null));
					}
					appArray.put(getTreeNode(entry0.getKey(), activityArray, 10, "white", activitycolor, pic));
				}
				packageArray.put(getTreeNode(entry.getKey(), appArray, 10, "white", packagecolor, null));
			}
			jsonArray.put(getTreeNode(projectId, packageArray, 10, "white", idcolor, null));
		}
		return jsonArray.toString().substring(1, jsonArray.toString().length() - 1);
	}

	public HashMap<String, Object> getTreeNode(String name, Object children, int value, String type, String level, String url) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("name", name);
		hashMap.put("children", children);
		hashMap.put("value", value);
		hashMap.put("type", type);
		hashMap.put("level", level);
		hashMap.put("url", url);
		return hashMap;
	}

	public HashMap<String, Object> getTreeEndNode(String name, Object children, int value, String type, String level, String detailparent, String detail, String url) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("name", name);
		hashMap.put("children", children);
		hashMap.put("value", value);
		hashMap.put("type", type);
		hashMap.put("level", level);
		hashMap.put("detailparent", detailparent);
		hashMap.put("detail", detail);
		hashMap.put("detailurl", url);
		return hashMap;
	}
}