package org.iii.security.platform.task;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.iii.security.platform.service.TaintdroidEmulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@ComponentScan(basePackages = {"org.iii.security.platform"})
@EnableScheduling
public class UEPlatformScheduledTask implements SchedulingConfigurer {
	
	@Autowired
	private TaintdroidEmulatorService emulatorLaunchService;
	
	@Bean(destroyMethod="shutdown")
    public Executor taskScheduler() {
        return Executors.newScheduledThreadPool(100);
    }
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(taskScheduler());
	}
	
	@Scheduled(fixedRate = 30000)
	public void extractAPKFile() {
		emulatorLaunchService.extractAPK();
	}
	
	@Scheduled(fixedRate = 10000)
	public void launchEmulatorTask() {
		emulatorLaunchService.launchEnv();
	}
	
	@Scheduled(fixedRate = 60000)
	public void launchLogcat() {
		emulatorLaunchService.launchLogcat();
	}
	
	@Scheduled(fixedRate = 30000)
	public void installAPK() {
		emulatorLaunchService.installAPK();
	}
	
	@Scheduled(fixedRate = 20000)
	public void analyzeLog() {
		emulatorLaunchService.analyzeLog();
	}

}
