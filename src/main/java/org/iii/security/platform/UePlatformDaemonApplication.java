package org.iii.security.platform;

import org.iii.security.platform.task.UEPlatformScheduledTask;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class UePlatformDaemonApplication {

    public static void main(String[] args) {
        SpringApplication.run(UEPlatformScheduledTask.class, args);
    }
    
}
