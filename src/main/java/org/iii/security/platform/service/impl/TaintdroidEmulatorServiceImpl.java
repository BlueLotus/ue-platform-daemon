package org.iii.security.platform.service.impl;

import java.io.FileNotFoundException;
import java.util.List;

import org.iii.security.platform.core.domain.APKManifestInfo;
import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.TreeDiagramResult;
import org.iii.security.platform.core.persistence.APKManifestInfoRepository;
import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.persistence.ProjectRepository;
import org.iii.security.platform.core.persistence.TreeDiagramResultRepository;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.iii.security.platform.process.APKFileInstallerProcess;
import org.iii.security.platform.process.APKManifestAnalyzerProcess;
import org.iii.security.platform.process.EmulatorLaunchProcess;
import org.iii.security.platform.process.LogcatLauncher;
import org.iii.security.platform.service.TaintdroidEmulatorService;
import org.iii.security.platform.util.TaintdroidLogParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class TaintdroidEmulatorServiceImpl implements TaintdroidEmulatorService {
	
	private static final Logger logger = LoggerFactory.getLogger(TaintdroidEmulatorServiceImpl.class);

	private static final int FIND_AVAILABLE_ENV = 0;
	private static final int FIND_LAUNCHED_ENV_WITHOUT_LOGCAT = 1;
	private static final int FIND_LAUNCHED_AND_LOGGED_ENV_WITHOUT_INSTALL_APK = 2;
	private static final int FIND_UNANALYZED_ENV = 3;
	
	private static final String MSG_FOR_FIND_AVAILABLE_ENV = "There is no any available emulator environments now, please try later...";
	private static final String MSG_FOR_FIND_LAUNCHED_ENV_WITHOUT_LOGCAT = "There is no any other emulator environments without logcat, please try later...";
	private static final String MSG_FOR_FIND_LAUNCHED_AND_LOGGED_ENV_WITHOUT_INSTALL_APK = "There is no emulator needs to install apk file, please try later...";
	private static final String MSG_FOR_FIND_UNANALYZED_ENV = "No analyzed env yet, please try later...";
	private static final String MSG_FOR_NO_ANALYTICS_RESULT = "The app is still analyzing, please check it later...";
	
	@Autowired
	private EmulatorEnvRepository emulatorEnvRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private TreeDiagramResultRepository treeDiagramResultRepository;
	
	@Autowired
	private APKManifestInfoRepository apkManifestInfoRepository;
	
	@Autowired
	private ProcessBuilderGenerator processBuilderGenerator;
	
	@Autowired
	private TaintdroidLogParser taintdroidLogParser;
	
	@Override
	public void extractAPK() {
		logger.debug("Finding all unextracted apk file...");
		List<Project> unextractedProjects = projectRepository.findByManifestExtractedIsFalse();
		APKManifestAnalyzerProcess apkManifestAnalyzerProcess = new APKManifestAnalyzerProcess(processBuilderGenerator);
		if(unextractedProjects.size() > 0) {
			logger.debug("Starting to extract apk file.");
			for (Project unextracted : unextractedProjects) {
				String unextractedApkName = unextracted.getApkName();
				logger.debug("Extracting apk file: {}", unextractedApkName);
				
				APKManifestInfo apkManifestInfo = apkManifestInfoRepository.findByProjectId(unextracted.getId());
				apkManifestAnalyzerProcess.decompileAPK(unextractedApkName);
				apkManifestAnalyzerProcess.extractAndroidManifest(unextractedApkName);
				apkManifestInfo.setUsesPermission(apkManifestAnalyzerProcess.getUsesPermission());
				apkManifestInfo.setPackageName(apkManifestAnalyzerProcess.getPackageName());
				apkManifestInfoRepository.save(apkManifestInfo);
				unextracted.setManifestExtracted(true);
				projectRepository.save(unextracted);
				logger.debug("Extract successfully.");
			}
		} else {
			logger.debug("No apk file needs to be extracted.");
		}
	}
	
	@Override
	public void launchEnv() {
		EmulationEnv availableEnv = findEnvWithSpecificSituation(FIND_AVAILABLE_ENV);
		if(availableEnv != null) {
			EmulatorLaunchProcess emulatorLaunchProcess = new EmulatorLaunchProcess(processBuilderGenerator);
			emulatorLaunchProcess.launchEmulator(availableEnv.getId(), emulatorEnvRepository);
			
			Project project = projectRepository.findOne(availableEnv.getAnalyticsProjectId());
			projectRepository.save(project.finished());
			emulatorEnvRepository.save(availableEnv.initialize());
			forciblyUpdateTreeDiagramResultForProject(project);
		}
	}

	@Override
	public void launchLogcat() {
		EmulationEnv envWithoutLogcat = findEnvWithSpecificSituation(FIND_LAUNCHED_ENV_WITHOUT_LOGCAT);
		if(envWithoutLogcat != null) {
			LogcatLauncher logcatLauncher = new LogcatLauncher(processBuilderGenerator);
			logcatLauncher.launchLogcat(envWithoutLogcat.getId(), emulatorEnvRepository);
		}
	}

	@Override
	public void installAPK() {
		EmulationEnv envWithoutInstallingApk = findEnvWithSpecificSituation(FIND_LAUNCHED_AND_LOGGED_ENV_WITHOUT_INSTALL_APK);
		if(envWithoutInstallingApk != null) {
			String packageName = apkManifestInfoRepository.findByProjectId(envWithoutInstallingApk.getAnalyticsProjectId()).getPackageName();
			APKFileInstallerProcess apkFileInstallerProcess = new APKFileInstallerProcess(processBuilderGenerator, packageName);
			if(apkFileInstallerProcess.install(envWithoutInstallingApk.getApkForAnalytics()) == EmulatorJobConstant.INSTALL_SUCCESSFULLY) {
				envWithoutInstallingApk.setApkInstalled(true);
				emulatorEnvRepository.save(envWithoutInstallingApk);
			}
		}
	}

	@Override
	public void analyzeLog() {
		EmulationEnv envWithApkReady = findEnvWithSpecificSituation(FIND_UNANALYZED_ENV);
		if(envWithApkReady != null) {
			try {
				taintdroidLogParser.initForEmulator(envWithApkReady);
				String analyzeResult = taintdroidLogParser.parseLog();
				if(resultIsGenerate(analyzeResult)) {
					if(resultIsNotMeaningful(analyzeResult)) {
						logger.debug("Still analyzing...");
					} else {
						long projectId = envWithApkReady.getAnalyticsProjectId();
						TreeDiagramResult treeDiagramResult = findTreeDiagramResult(projectId);
						treeDiagramResult.setResult(analyzeResult);
						treeDiagramResultRepository.save(treeDiagramResult);
						logger.debug("Updated analytics result for project: {}.", projectId);
					}
				} else {
					logger.debug(MSG_FOR_NO_ANALYTICS_RESULT);
				}
			} catch(FileNotFoundException e) {
				logger.debug("No log file found in the specified directory, please check the directory...");
			} catch (NullPointerException e) {
				logger.debug("Log file might not have any content, please check the file directly...");
			}catch (Exception e) {
				logger.debug(e.getMessage());
			}
		}
	}
	
	private EmulationEnv findEnvWithSpecificSituation(int condition) {
		EmulationEnv specificEmulationEnv = null;
		List<EmulationEnv> envs = null;
		String resultIsEmptyMsg = null;
		
		if(condition == FIND_AVAILABLE_ENV) {
			envs = emulatorEnvRepository.findByScheduledIsTrueAndEmulatorLaunchedIsFalse();
			resultIsEmptyMsg = MSG_FOR_FIND_AVAILABLE_ENV;
		} else if(condition == FIND_LAUNCHED_ENV_WITHOUT_LOGCAT) {
			envs = emulatorEnvRepository.findByScheduledIsTrueAndEmulatorLaunchedIsTrue();
			resultIsEmptyMsg = MSG_FOR_FIND_LAUNCHED_ENV_WITHOUT_LOGCAT;
		} else if(condition == FIND_LAUNCHED_AND_LOGGED_ENV_WITHOUT_INSTALL_APK) {
			envs = emulatorEnvRepository.findByScheduledIsTrueAndEmulatorLaunchedIsTrueAndLogcatLaunchedIsTrueAndCanInstallApkIsTrueAndApkInstalledIsFalse();
			resultIsEmptyMsg = MSG_FOR_FIND_LAUNCHED_AND_LOGGED_ENV_WITHOUT_INSTALL_APK;
		} else if(condition == FIND_UNANALYZED_ENV) {
			envs = emulatorEnvRepository.findByScheduledIsTrueAndEmulatorLaunchedIsTrueAndLogcatLaunchedIsTrueAndCanInstallApkIsTrueAndApkInstalledIsTrue();
			resultIsEmptyMsg = MSG_FOR_FIND_UNANALYZED_ENV;
		}
		
		if(envs.isEmpty()) {
			logger.debug(resultIsEmptyMsg);
		} else {
			specificEmulationEnv = envs.get(0);
		}
		
		return specificEmulationEnv;
	}
	
	private boolean resultIsGenerate(String analyzeResult) {
		boolean result = true;
		if(analyzeResult.contains(EmulatorJobConstant.DEFAULT_TREE_DIAGRAM_RESULT)) {
			result = false;
		}
		return result;
	}
	
	private TreeDiagramResult findTreeDiagramResult(long projectId) {
		List<TreeDiagramResult> results = treeDiagramResultRepository.findByProjectId(projectId);
		TreeDiagramResult result = null;
		if(results.size() != 0) {
			result = results.get(0);
		}
		return result;
	}
	
	private void forciblyUpdateTreeDiagramResultForProject(Project project) {
		try {
			taintdroidLogParser.initForProject(project);
			TreeDiagramResult treeDiagramResult = findTreeDiagramResult(project.getId());
			treeDiagramResult.setResult(taintdroidLogParser.parseLog());
			treeDiagramResultRepository.save(treeDiagramResult);
			logger.debug("Analytics for project '{}' has been finished successfully, please check the diagram result.", project.getProjectName());
		} catch (Exception e) {
			logger.debug("Forcibly update tree diagram result for project '{}' failed, error message: {}", project.getProjectName(), e.getMessage());
		}
	}
	
	private boolean resultIsNotMeaningful(String analyzeResult) {
		boolean meaningful = analyzeResult.contentEquals("") || 
				analyzeResult.contains(EmulatorJobConstant.STILL_ANALYZING) || 
				analyzeResult.contains(EmulatorJobConstant.DEFAULT_TREE_DIAGRAM_RESULT);
		return meaningful; 
	}

}
