package org.iii.security.platform.service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface TaintdroidEmulatorService {
	
	void extractAPK();
	
	void launchEnv();
	
	void launchLogcat();
	
	void installAPK();
	
	void analyzeLog();
	
}
